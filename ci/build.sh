#!/bin/bash

set -e -x

ls -la

export TERM=${TERM:-dumb}


cd source

./gradlew -x test --no-daemon build

cp -r . ../build-out


